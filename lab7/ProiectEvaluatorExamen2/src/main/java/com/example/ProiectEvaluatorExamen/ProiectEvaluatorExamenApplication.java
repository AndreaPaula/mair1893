package com.example.ProiectEvaluatorExamen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProiectEvaluatorExamenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProiectEvaluatorExamenApplication.class, args);
	}
}
