package com.example.ProiectEvaluatorExamen.exception;

public class DuplicateQuestionException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DuplicateQuestionException(String message) {
		super(message);
	}

}
