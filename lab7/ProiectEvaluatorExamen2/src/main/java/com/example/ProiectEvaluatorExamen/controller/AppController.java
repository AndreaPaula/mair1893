package com.example.ProiectEvaluatorExamen.controller;

import java.util.LinkedList;
import java.util.List;

import com.example.ProiectEvaluatorExamen.exception.DuplicateQuestionException;
import com.example.ProiectEvaluatorExamen.exception.NotAbleToCreateStatisticsException;
import com.example.ProiectEvaluatorExamen.exception.NotAbleToCreateTestException;
import com.example.ProiectEvaluatorExamen.model.Intrebare;
import com.example.ProiectEvaluatorExamen.model.Statistica;
import com.example.ProiectEvaluatorExamen.model.Test;
import com.example.ProiectEvaluatorExamen.repository.IntrebariRepository;


public class AppController {
	/**
	 Clasa AppController are responsabilitatea de a face legatura dintre Repository si StartApp si in aceasta clasa este si metoda prin care se creaza un test nou
	 */
	
	private IntrebariRepository intrebariRepository;
	
	public AppController(String file) {

		intrebariRepository = new IntrebariRepository(file);
		loadIntrebariFromFile(file);
	}
	
	public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateQuestionException {
		
		intrebariRepository.addIntrebare(intrebare);
		return intrebare;
	}
	
	public boolean exists(Intrebare intrebare){
		return intrebariRepository.exists(intrebare);
	}
	
	public Test createNewTest() throws NotAbleToCreateTestException {
		
		if(intrebariRepository.getIntrebari().size() < 5) {
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
		}else{
		if(intrebariRepository.getNumberOfDistinctDomains() < 5) {
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");
		}else {
			List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
			List<String> domenii = new LinkedList<String>();
			Intrebare intrebare;
			Test test = new Test();

			while (testIntrebari.size() != 5) {
				intrebare = intrebariRepository.pickRandomIntrebare();

				if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
					testIntrebari.add(intrebare);
					domenii.add(intrebare.getDomeniu());
				}

			}

			test.setIntrebari(testIntrebari);
			return test;
		}
	}
	}
	
	public void loadIntrebariFromFile(String f){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
	}
	public List<Intrebare> getIntrebari(){
		return  intrebariRepository.getIntrebari();
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException {
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			//statistica.add(domeniu, intrebariRepository.getIntrebari().size());
			statistica.add(domeniu,intrebariRepository.getNumberOfIntrebariByDomain(domeniu));
		}
		
		return statistica;
	}

}
