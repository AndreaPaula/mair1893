package com.example.ProiectEvaluatorExamen.repository;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


import com.example.ProiectEvaluatorExamen.exception.DuplicateQuestionException;
import com.example.ProiectEvaluatorExamen.exception.InputValidationFailedException;
import com.example.ProiectEvaluatorExamen.model.Intrebare;
import com.example.ProiectEvaluatorExamen.util.InputValidation;

public class IntrebariRepository {
	
	private List<Intrebare> intrebari;
	private String fileName;
	
	public IntrebariRepository(String fileName)
	{
		this.fileName = fileName;
		setIntrebari(new LinkedList<Intrebare>());
	}
	
	public String addIntrebare(Intrebare i)  {
		if(exists(i))
			try {
				throw new DuplicateQuestionException("Intrebarea deja exista!");
			} catch (DuplicateQuestionException e) {
				e.printStackTrace();
			}
		intrebari.add(i);
		saveIntrebareToFile(i,fileName);
		return "quiz added";
	}

	private void saveIntrebareToFile(Intrebare i,String fileName) {
		try(FileWriter fw = new FileWriter(fileName, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw))
		{
			out.println(i.getEnunt() + "\n" + i.getVarianta1() + "\n" + i.getVarianta2() + "\n\n" + i.getVariantaCorecta() + "\n" + i.getDomeniu() + "\n" + "##");
		} catch (IOException e) {
			//exception handling left as an exercise for the reader
		}
	}

	public boolean exists(Intrebare i){
		for(Intrebare intrebare : intrebari)
			if(intrebare.equals(i))
				return true;
		return false;
	}
	
	public Intrebare pickRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();
		
	}
	
	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}
	
	public List<Intrebare> getIntrebariByDomain(String domain){
		List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebare);
			}
		}
		
		return intrebariByDomain;
	}
	
	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}
	
	public List<Intrebare> loadIntrebariFromFile(String f) {
		
		List<Intrebare> intrebari = new LinkedList<Intrebare>();
		BufferedReader br = null; 
		String line = null;
		List<String> intrebareAux;
		Intrebare intrebare;
		
		try{
			br = new BufferedReader(new FileReader(f));
			line = br.readLine();
			while(line != null){
				intrebareAux = new LinkedList<String>();
				while(!line.equals("##")){
					intrebareAux.add(line);
					line = br.readLine();
				}
				intrebare = new Intrebare();

//				intrebare.setEnunt(intrebareAux.get(0));
//				intrebare.setVarianta1(intrebareAux.get(1));
//				intrebare.setVarianta2(intrebareAux.get(2));
//				intrebare.setVariantaCorecta(intrebareAux.get(4));
//				intrebare.setDomeniu(intrebareAux.get(5));

				String enunt = intrebareAux.get(0);
				String varianta1 = intrebareAux.get(1);
				String varianta2 = intrebareAux.get(2);
				String variantaCorecta = intrebareAux.get(4);
				String domeniu = intrebareAux.get(5);
				InputValidation.validateEnunt(enunt);
				InputValidation.validateVarianta1(varianta1);
				InputValidation.validateVarianta2(varianta2);
				InputValidation.validateVariantaCorecta(variantaCorecta);
				InputValidation.validateDomeniu(domeniu);
				intrebare.setEnunt(enunt);
				intrebare.setVarianta1(varianta1);
				intrebare.setVarianta2(varianta2);
				intrebare.setVariantaCorecta(variantaCorecta);
				intrebare.setDomeniu(domeniu);
				intrebari.add(intrebare);
				line = br.readLine();
			}
		
		}
		catch (IOException e) {
			// TODO: handle exception
		} catch (InputValidationFailedException e) {
			e.printStackTrace();
		} finally{
			try {
				br.close();
			} catch (IOException e) {
				// TODO: handle exception
			}
		}
		
		return intrebari;
	}
	
	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
	
}
