package com.example.ProiectEvaluatorExamen;


//import javax.servlet.annotation.WebServlet;
//
//import com.example.ProiectEvaluatorExamen.controller.AppController;
//import com.example.ProiectEvaluatorExamen.model.Intrebare;
//import com.vaadin.annotations.Theme;
//import com.vaadin.annotations.VaadinServletConfiguration;
//import com.vaadin.server.VaadinRequest;
//import com.vaadin.server.VaadinServlet;
//import com.vaadin.ui.Button;
//import com.vaadin.ui.Label;
//import com.vaadin.ui.TextField;
//import com.vaadin.ui.UI;
//import com.vaadin.ui.VerticalLayout;
//
//import java.util.Arrays;
//
///**
// * This UI is the application entry point. A UI may either represent a browser window
// * (or tab) or some part of an HTML page where a Vaadin application is embedded.
// * <p>
// * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
// * overridden to add component to the user interface and initialize non-component functionality.
// */
//@Theme("mytheme")
//public class EvaluatorUI extends UI {
//
//    AppController controller =new AppController("intrebari.txt");
//    Intrebare intrebare=new Intrebare();
//
//    @Override
//    protected void init(VaadinRequest vaadinRequest) {
//        final VerticalLayout layout = new VerticalLayout();
//
//        layout.setCaption("Adauga o intrebare");
//
//        final TextField enuntField = new TextField();
//        enuntField.setCaption("enunt");
//        enuntField.setId("enuntField");
//
//        final TextField var1Field = new TextField();
//        var1Field.setCaption("var1");
//        var1Field.setId("var1Field");
//
//
//        final TextField var2Field = new TextField();
//        var2Field.setCaption("var2");
//        var2Field.setId("var2Field");
//
//        final TextField varCorectField = new TextField();
//        varCorectField.setCaption("corect");
//        varCorectField.setId("varCorectField");
//
//        final TextField domeniuField=new TextField();
//        domeniuField.setCaption("domeniu");
//        domeniuField.setId("domeniuField");
//
//
//        Button button = new Button("Adauga intrebare");
//        button.setId("addButton");
//        button.addClickListener(e -> {
//            intrebare.setEnunt(enuntField.getValue());
//            intrebare.setVarianta1(var1Field.getValue());
//            intrebare.setVarianta2(var2Field.getValue());
//            intrebare.setVariantaCorecta(varCorectField.getValue());
//            intrebare.setDomeniu(domeniuField.getValue());
//            try {
//                controller.addNewIntrebare(intrebare);
//                Label label1=new Label("intrebare adaugata");
//                label1.setId("result");
//                layout.addComponent(label1);
//            }
//            catch(Exception ex)
//            {
//                Label label2=new Label("intrebare gresita");
//                label2.setId("result");
//                layout.addComponent(label2);
//            }
//        });
//
//        layout.addComponents(enuntField,var1Field,var2Field,varCorectField,domeniuField ,button);
//
//        setContent(layout);
//    }

//    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
//    @VaadinServletConfiguration(ui = EvaluatorUI.class, productionMode = false)
//    public static class MyUIServlet extends VaadinServlet {
//    }
//}

import com.example.ProiectEvaluatorExamen.exception.InputValidationFailedException;
import com.example.ProiectEvaluatorExamen.model.Intrebare;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
@Theme("valo")
public class EvaluatorUI extends UI {

    private VerticalLayout layout;

    private Label result;

    @Autowired
    IntrebariList intrebariList;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setupLayout();
        addHeader();
        addForm();
       // addIntrebareList();
        //addActionButton();
    }

    private void addActionButton() {

    }

    private void addIntrebareList() {
        layout.addComponent(intrebariList);

    }

    private void addForm() {
        HorizontalLayout formLayout = new HorizontalLayout();
        TextField enuntField = new TextField();
        enuntField.setId("enuntField");
        TextField var1Field = new TextField();
        var1Field.setId("var1Field");
        TextField var2Field = new TextField();
        var2Field.setId("var2Field");
        TextField varCorectField = new TextField();
        varCorectField.setId("varCorectField");
        TextField domeniuField = new TextField();
        domeniuField.setId("domeniuField");
        Button addButton = new Button("Adauga intrebare");
        addButton.setId("addButton");
        formLayout.addComponents(enuntField,var1Field,var2Field,varCorectField,domeniuField,addButton);
        layout.addComponent(formLayout);

        addButton.addClickListener(click -> {
            try {
               intrebariList.add(new Intrebare(enuntField.getValue(),var1Field.getValue(),var2Field.getValue(),varCorectField.getValue(),domeniuField.getValue()));
               enuntField.clear();
               enuntField.focus();
               var1Field.clear();
               var1Field.focus();
               var2Field.clear();
               var2Field.focus();
               varCorectField.clear();
               varCorectField.focus();
               domeniuField.clear();
               domeniuField.focus();
                //Label label1=new Label("Intrebarea a fost adaugata ");
                //label1.setId("result");
                result.setValue("intrebare adaugata");
            } catch (InputValidationFailedException e) {
                //e.printStackTrace();
                //Label label2=new Label("Intrebare invalida");
                //label2.setId("result");
                //layout.addComponent(label2);
                result.setValue("intrebare gresita");
            }
        });

    }

    private void addHeader() {
        Label header = new Label("Evaluator");
        layout.addComponent(header);
        layout.addComponent(result);

    }

    private void setupLayout(){
        layout = new VerticalLayout();
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(layout);
        result = new Label();
        result.setId("result");
    }
}
