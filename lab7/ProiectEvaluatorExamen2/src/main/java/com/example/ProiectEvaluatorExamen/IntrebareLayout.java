package com.example.ProiectEvaluatorExamen;

import com.example.ProiectEvaluatorExamen.model.Intrebare;
import com.vaadin.ui.*;

public class IntrebareLayout extends HorizontalLayout {

    private final Label text;

    public IntrebareLayout(Intrebare intrebare) {
        setSpacing(true);
        setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        text = new Label();
        text.setValue(intrebare.getEnunt());

        addComponent(text);
    }
}

