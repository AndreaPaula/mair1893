package com.example.ProiectEvaluatorExamen;

import com.example.ProiectEvaluatorExamen.controller.AppController;
import com.example.ProiectEvaluatorExamen.exception.DuplicateQuestionException;
import com.example.ProiectEvaluatorExamen.model.Intrebare;
import com.example.ProiectEvaluatorExamen.repository.IntrebariRepository;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class IntrebariList extends VerticalLayout{

//    IntrebariRepository repository;

    AppController controller;
    @PostConstruct
    void init(){
        controller = new AppController("intrebari.txt");
        setSpacing(true);
       // setIntrebari(controller.getIntrebari());
        update();
    }

    private void update() {
        setIntrebari(controller.getIntrebari());
    }


    private void setIntrebari(List<Intrebare> intrebari) {
        removeAllComponents();
        intrebari.forEach(intrebare->{
            addComponent(new IntrebareLayout(intrebare));
        });
    }

    public void add(Intrebare intrebare) {
        try {
            controller.addNewIntrebare(intrebare);
            update();
        } catch (DuplicateQuestionException e) {
            e.printStackTrace();
        }
    }


}
