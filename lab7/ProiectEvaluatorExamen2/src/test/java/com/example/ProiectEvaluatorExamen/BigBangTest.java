package com.example.ProiectEvaluatorExamen;

import org.junit.Test;
import org.junit.runner.JUnitCore;

public class BigBangTest
{


    @Test
    public  void testUnitaraA(){
        JUnitCore.runClasses(BVA_Test.class);
        JUnitCore.runClasses(ECP_Test.class);
    }

    @Test
    public  void testUnitaraB(){
        JUnitCore.runClasses(WBT_Test.class);

    }

    @Test
    public  void testUnitaraC(){
        JUnitCore.runClasses(WBT4_Test.class);

    }


    @Test
    public  void testIntegrareP(){
        testUnitaraA();
        testUnitaraB();
        testUnitaraC();

    }

}
