package com.example.ProiectEvaluatorExamen;


import com.example.ProiectEvaluatorExamen.exception.DuplicateQuestionException;
import com.example.ProiectEvaluatorExamen.exception.InputValidationFailedException;
import com.example.ProiectEvaluatorExamen.model.Intrebare;
import com.example.ProiectEvaluatorExamen.repository.IntrebariRepository;
import org.junit.Test;

public class BVA_Test {
    IntrebariRepository repo = new IntrebariRepository("intrebari.txt");

    @Test
    public void TC3_BVA() throws InputValidationFailedException, DuplicateQuestionException {
        Intrebare intrebare = new Intrebare("M?","1)v1","2)v2","1","Fizica");
        String result = repo.addIntrebare(intrebare);
        assert (result=="quiz added");

    }

    @Test
    public void TC9_BVA() throws InputValidationFailedException, DuplicateQuestionException {
        Intrebare intrebare = new Intrebare("Enunt?","1)V","2)v2","1","Fizica");
        String result = repo.addIntrebare(intrebare);
        assert (result=="quiz added");
    }

    @Test
    public void TC15_BVA() throws InputValidationFailedException, DuplicateQuestionException {
        Intrebare intrebare = new Intrebare("Enunt?","1)v1","2)V","1","Fizica");
        String result = repo.addIntrebare(intrebare);
        assert (result=="quiz added");
    }
    @Test
    public void TC19_BVA() throws InputValidationFailedException, DuplicateQuestionException {
        Intrebare intrebare = new Intrebare("Enunt?","1)v1","2)v2","1","Fizica");
        String result = repo.addIntrebare(intrebare);
        assert (result=="quiz added");
    }
    @Test
    public void TC27_BVA() throws InputValidationFailedException, DuplicateQuestionException {
        Intrebare intrebare = new Intrebare("Enunt?","1)v1","2)v2","1","D");
        String result = repo.addIntrebare(intrebare);
        assert (result=="quiz added");
    }

    @Test
    public void TC20_BVA() throws InputValidationFailedException, DuplicateQuestionException {
        try {
            Intrebare intrebare = new Intrebare("Enunt?", "1)v1", "2)v2", "4", "D");
            String result = repo.addIntrebare(intrebare);
        }
        catch (Exception error){assert (error.getMessage().equals("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}"));}
    }



}
