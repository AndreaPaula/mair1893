package com.example.ProiectEvaluatorExamen;


import com.example.ProiectEvaluatorExamen.exception.DuplicateQuestionException;
import com.example.ProiectEvaluatorExamen.exception.InputValidationFailedException;
import com.example.ProiectEvaluatorExamen.model.Intrebare;
import com.example.ProiectEvaluatorExamen.repository.IntrebariRepository;
import org.junit.Test;

public class ECP_Test {
    IntrebariRepository repo = new IntrebariRepository("intrebari.txt");
    @Test
    public void TC1_ECP() throws InputValidationFailedException, DuplicateQuestionException {

        Intrebare intrebare = new Intrebare("Cat e ceasul?","1)12:00","2)13:00","1","Timp");
        String result = repo.addIntrebare(intrebare);
        assert(result == "quiz added");
    }
    @Test(expected = InputValidationFailedException.class)
    public void TC11_ECP() throws InputValidationFailedException, DuplicateQuestionException {

        Intrebare intrebare = new Intrebare("","1)12:00","2)13:00","1","Timp");
        String result = repo.addIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC2_ECP() throws InputValidationFailedException, DuplicateQuestionException {

        Intrebare intrebare = new Intrebare("cat e ceasul?","1)12:00","2)13:00","1","Timp");
        String result = repo.addIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC12_ECP() throws InputValidationFailedException, DuplicateQuestionException {

        Intrebare intrebare = new Intrebare("Cat e ceasul?","","2)14:00","1","Timp");
        String result = repo.addIntrebare(intrebare);
    }
    @Test(expected = InputValidationFailedException.class)
    public void TC13_ECP() throws InputValidationFailedException, DuplicateQuestionException {

        Intrebare intrebare = new Intrebare("Cat e ceasul?","1)12:00","","1","Timp");
        String result = repo.addIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC14_ECP() throws InputValidationFailedException, DuplicateQuestionException {

        Intrebare intrebare = new Intrebare("Cat e ceasul?","1)13:00","2)15:00","","Timp");
        String result = repo.addIntrebare(intrebare);
    }




}
