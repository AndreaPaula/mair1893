package com.example.ProiectEvaluatorExamen;

import com.example.ProiectEvaluatorExamen.controller.AppController;

import org.junit.Test;

public class WBT_Test {

    @Test
    public void Req02_TC01() {
        try {
            AppController appController = new AppController("intr.txt");
            com.example.ProiectEvaluatorExamen.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Nu exista suficiente intrebari pentru crearea unui test!(5)"));
        }
    }

    @Test
    public void Req02_TC02() {
        try {
            AppController appController = new AppController("dom.txt");
            com.example.ProiectEvaluatorExamen.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Nu exista suficiente domenii pentru crearea unui test!(5)"));
        }
    }

    @Test
    public void Req02_TC03() {
        try {
            AppController appController = new AppController("intrebari.txt");
            com.example.ProiectEvaluatorExamen.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Test
    public void Req02_TCO4(){
        try {
            AppController appController = new AppController("date.txt");
            com.example.ProiectEvaluatorExamen.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}