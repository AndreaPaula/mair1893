package Lab07.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("http://localhost:8081")
public class DictionaryPage extends PageObject {

    @FindBy(id="enuntField")
    private WebElementFacade enunt;

    @FindBy(id="var1Field")
    private WebElementFacade var1;

    @FindBy(id="var2Field")
    private WebElementFacade var2;

    @FindBy(id="varCorectField")
    private WebElementFacade varCorecta;

    @FindBy(id="domeniuField")
    private WebElementFacade domeniu;

    @FindBy(id="addButton")
    private WebElementFacade addButton;

    public void enter_enunt(String keyword){
        enunt.type(keyword);
    }
    public void enter_var1(String keyword){
        var1.type(keyword);
    }
    public void enter_var2(String keyword){
        var2.type(keyword);
    }
    public void enter_varCorecta(String keyword){
        varCorecta.type(keyword);
    }
    public void enter_domeniu(String keyword){
        domeniu.type(keyword);
    }

    public void add(){
        addButton.click();
    }

    public String getResult(){
        WebElementFacade result = find(By.id("result"));
        return result.getText();
    }

}