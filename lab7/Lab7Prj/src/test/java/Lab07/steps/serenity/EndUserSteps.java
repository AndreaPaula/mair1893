package Lab07.steps.serenity;

import Lab07.pages.DictionaryPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    @Step
    public void enters(String enunt, String var1, String var2,String varCorecta, String domeniu) {

        dictionaryPage.enter_enunt(enunt);
        dictionaryPage.enter_var1(var1);
        dictionaryPage.enter_var2(var2);
        dictionaryPage.enter_varCorecta(varCorecta);
        dictionaryPage.enter_domeniu(domeniu);
    }

    @Step
    public void starts_search()
    {
        dictionaryPage.add();
    }

    @Step
    public void should_see_definition(String definition) {
        assertThat(dictionaryPage.getResult(), containsString(definition));
    }

    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }

//    @Step
//    public void looks_for(String term) {
//        enters(term);
//        starts_search();
//    }
    @Step
    public void adds(String enunt, String var1, String var2, String corecta, String domeniu) {
        enters(enunt, var1, var2 , corecta, domeniu);
        starts_search();
    }
}