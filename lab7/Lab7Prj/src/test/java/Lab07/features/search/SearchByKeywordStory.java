package Lab07.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import Lab07.steps.serenity.EndUserSteps;

import java.util.Random;

@RunWith(SerenityRunner.class)
public class SearchByKeywordStory {

    @Managed(uniqueSession = true)
    public WebDriver webDriver;

    @Steps
    public EndUserSteps test;

    @Issue("#TEST-1")
    @Test
    public void adaugaIntrebareValida() {
        test.is_the_home_page();
        Random rand = new Random();
        int x = rand.nextInt(100);
        int y = rand.nextInt(100);
        String enunt = "Ce faci " + x + "ddd" + y + "?";
        test.adds( enunt,"1)ok","2)buuun","1","Bla");
        test.should_see_definition("intrebare adaugata");
    }

    @Issue("#TEST-2")
    @Test
    public void adaugaIntrebareNonvalida()
    {
        test.is_the_home_page();
        test.adds("cccc", "", "","","");
        test.should_see_definition("intrebare gresita");
    }


//    @Managed(uniqueSession = true)
//    public WebDriver webdriver;
//
//    @Steps
//    public EndUserSteps anna;
//
//    @Issue("#WIKI-1")
//    @Test
//    public void searching_by_keyword_apple_should_display_the_corresponding_article() {
//        anna.is_the_home_page();
//        anna.looks_for("apple");
//        anna.should_see_definition("A common, round fruit produced by the tree Malus domestica, cultivated in temperate climates.");
//
//    }
//
//    @Test
//    public void searching_by_keyword_banana_should_display_the_corresponding_article() {
//        anna.is_the_home_page();
//        anna.looks_for("pear");
//        anna.should_see_definition("An edible fruit produced by the pear tree, similar to an apple but elongated towards the stem.");
//    }
//
//    @Pending @Test
//    public void searching_by_ambiguious_keyword_should_display_the_disambiguation_page() {
//    }
} 