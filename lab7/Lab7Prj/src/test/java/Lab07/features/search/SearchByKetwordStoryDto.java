package Lab07.features.search;

import Lab07.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

//@RunWith(SerenityParameterizedRunner.class)
//@UseTestDataFrom("src/test/resources/WikiTest.csv")
@RunWith(SerenityRunner.class)
public class SearchByKetwordStoryDto {

    @Managed(uniqueSession = true)
    public WebDriver webDriver;

    @ManagedPages(defaultUrl = "http://localhost:8081")
    public Pages pages;

    private String enunt;
    private String var1;
    private String var2;
    private String varCorecta;
    private String domeniu;

//    @Qualifier
//    public String getQualifier(){
//        return enunt;
//    }
    
    @Steps
    public EndUserSteps endUser;

    @Issue("#TEST-2")
    @Test
    public void dateDinCSV(){
        parseCSVFile();
        endUser.is_the_home_page();
        endUser.adds(getEnunt(),getVar1(),getVar2(),getVarCorecta(),getDomeniu()
        );
        endUser.should_see_definition("intrebare gresita");
    }

    public String getDomeniu() {
        return domeniu;
    }

    public String getEnunt() {
        return enunt; 
    }

    public String getVar1() {
        return var1;
    }

    public String getVar2() {
        return var2;
    }

    public String getVarCorecta() {
        return varCorecta;
    }

    public void parseCSVFile(){
        String csvFile = "src/test/resources/WikiTest.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] data = line.split(cvsSplitBy);

                enunt = data[0];
                var1 = data[1];
                var2 = data[2];
                varCorecta = data[3];
                domeniu = data[4];

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    }

