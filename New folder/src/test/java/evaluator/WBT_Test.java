package evaluator;

import evaluator.controller.AppController;
import org.junit.Test;

public class WBT_Test {

    @Test
    public void Req02_TC01() {
        try {
            AppController appController = new AppController("intr.txt");
            evaluator.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Nu exista suficiente intrebari pentru crearea unui test!(5)"));
        }
    }

    @Test
    public void Req02_TC02() {
        try {
            AppController appController = new AppController("dom.txt");
            evaluator.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Nu exista suficiente domenii pentru crearea unui test!(5)"));
        }
    }

    @Test
    public void Req02_TC03() {
        try {
            AppController appController = new AppController("intrebari.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Test
    public void Req02_TCO4(){
        try {
            AppController appController = new AppController("date.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}