package evaluator;

import evaluator.controller.AppController;
import evaluator.model.Statistica;
import org.junit.Test;

public class WBT4_Test {

    @Test
    public void TestEmptyFileNonValid() {
        try {
            AppController appController = new AppController("gol.txt");
            Statistica statistica = appController.getStatistica();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Repository-ul nu contine nicio intrebare!"));
        }
    }

    @Test
    public void TestValid() {
        try {
            AppController appController = new AppController("intrebari.txt");
            Statistica statistica = appController.getStatistica();
            assert (statistica.getIntrebariDomenii().isEmpty() == false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}