package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController(file);
		
		boolean activ = true;
		String optiune = null;
		Scanner scanner = new Scanner(System.in);
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				try {
					System.out.println("Introduceti enuntul intrebarii: ");
					String enunt = scanner.nextLine();
					System.out.println("Introduceti prima varianta de raspuns: ");
					String varianta1 = scanner.nextLine();
					System.out.println("Introduceti a doua varianta de raspuns: ");
					String varianta2 = scanner.nextLine();
					System.out.println("Introduceti varianta corecta de raspuns : ");
					String variantaCorecta = scanner.nextLine();
					System.out.println("Introduceti domeniul intrebarii: ");
					String domeniu = scanner.nextLine();
					Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, variantaCorecta, domeniu);
					appController.addNewIntrebare(intrebare);

				} catch (Exception e) {
					//e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case "2" :
				try {
					Test test = appController.createNewTest();
					System.out.println("S-a creat un test "  + test.toString());
				} catch (NotAbleToCreateTestException e) {
					//e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case "3" :
				appController.loadIntrebariFromFile(file);
				//Statistica statistica;
				try {
				//	statistica = appController.getStatistica();
					System.out.println(appController.getStatistica());
				} catch (NotAbleToCreateStatisticsException e) {
					// TODO
					//e.printStackTrace();
					System.out.println(e.getMessage());
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
